SHELL := /bin/bash

run:
	go run cmd/main.go -f "configFile=config"

lint:
	golangci-lint version
	golangci-lint linters
	golangci-lint run

# ==============================================================================
# Building containers

VERSION := 1.0
DOCKER_IMAGE := "my-street-route-planner"

all: service

service:
	docker build \
		-f deployment/docker/my-street-route-planner-api.dockerfile \
		-t $(DOCKER_IMAGE):$(VERSION) \
		--build-arg BUILD_REF=$(VERSION) \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		.

# ==============================================================================
# Running from within k8s/kind

KIND_CLUSTER := my-street-route-cluster

kind-up:
	kind create cluster \
		--image kindest/node:v1.21.1@sha256:69860bda5563ac81e3c0057d654b5253219618a22ec3a346306239bba8cfa1a6 \
		--name $(KIND_CLUSTER) \
		--config deployment/k8s/kind/kind-config.yaml
	kubectl config set-context --current --namespace=street-route-planner-system

kind-down:
	kind delete cluster --name $(KIND_CLUSTER)

kind-load:
	kind load docker-image $(DOCKER_IMAGE):$(VERSION) --name $(KIND_CLUSTER)

kind-apply:
	kustomize build deployment/k8s/kind/my-street-route-planner-pod | kubectl apply -f -

kind-status:
	kubectl get nodes -o wide
	kubectl get svc -o wide
	kubectl get pods -o wide --watch --all-namespaces

kind-status-service:
	kubectl get pods -o wide --watch

kind-logs:
	kubectl logs -l app=street-route-planner --all-containers=true -f --tail=100

kind-restart:
	kubectl rollout restart deployment street-route-planner-pod

kind-update: all kind-load kind-restart

kind-update-apply: all kind-load kind-apply

kind-describe:
	kubectl describe pod -l app=street-route-planner

# ==============================================================================
# Modules support

tidy:
	go mod tidy
	go mod vendor