ARG GO_VERSION=1.17

# Build the Go Binary.
FROM golang:${GO_VERSION}-alpine AS builder
RUN apk update && apk add alpine-sdk git && rm -rf /var/cache/apk/*

ENV CGO_ENABLED 0
ARG BUILD_REF

# Copy the source code into the container.
COPY . /service

# Build the service binary.
WORKDIR /service/cmd
RUN go build -o main -ldflags "-X main.build=${BUILD_REF}"

# Run the Go Binary in Alpine.
FROM alpine:latest
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

ARG BUILD_DATE
ARG BUILD_REF

COPY --from=builder /service /service/service
WORKDIR /service/service/cmd

CMD ["./main"]

LABEL org.opencontainers.image.created="${BUILD_DATE}" \
      org.opencontainers.image.title="my-street-route-planner-api" \