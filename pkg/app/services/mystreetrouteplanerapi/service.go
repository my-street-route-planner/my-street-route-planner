package mystreetrouteplanerapi

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/my-street-route-planner/cmd/config"
	"gitlab.com/my-street-route-planner/internal/business/web/middleware"
	"gitlab.com/my-street-route-planner/pkg/app/services/mystreetrouteplanerapi/handlers"
	"go.uber.org/zap"
)

// Run run a services api.
func Run(log *zap.SugaredLogger, cfg config.Config) error {
	log.Infow("starting service", "version", "develop")
	log.Infow("configuration", "cfg", cfg)

	app := echo.New()

	// add middleware.
	app.Use(middleware.GenerateTraceID())
	app.Use(middleware.ZapLogger(log))

	// init handlers.
	handlers.Handlers(app, handlers.HandlerConfig{Log: log})

	log.Errorf("%s", app.Start(":5000"))

	return nil
}
