// Package handlers contains the full set of handler functions and routes supported by the web api.
package handlers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/my-street-route-planner/pkg/app/services/mystreetrouteplanerapi/handlers/v1/health"
	"go.uber.org/zap"
)

// HandlerConfig contains all the mandatory systems required by handlers.
type HandlerConfig struct {
	Log *zap.SugaredLogger
}

// Handlers  constructs application routes defined.
func Handlers(app *echo.Echo, cfg HandlerConfig) {
	// init handler health.
	handlerHealth := health.Handlers{
		Log: cfg.Log,
	}

	app.GET("v1/health", handlerHealth.Health)
}
