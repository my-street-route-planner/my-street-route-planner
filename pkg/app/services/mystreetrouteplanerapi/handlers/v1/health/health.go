// Package health contains all the health handlers.
package health

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

// Handlers manages the set of check endpoints.
type Handlers struct {
	Log *zap.SugaredLogger
}

func (h Handlers) Health(ctx echo.Context) error {
	h.Log.Infow("EndPoint", ctx.Request().RequestURI, "start")
	h.Log.Infow("EndPoint", ctx.Request().RequestURI, "end")

	return fmt.Errorf("%w", ctx.JSON(http.StatusOK, map[string]interface{}{"status": "Ok!"}))
}
