package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/my-street-route-planner/cmd/config"
	"gitlab.com/my-street-route-planner/internal/foundation/logger"
	"gitlab.com/my-street-route-planner/pkg/app/services/mystreetrouteplanerapi"
	"go.uber.org/zap"
)

const errorChan = 2

func main() {
	// init logger which zap library.
	log, err := logger.NewLogger("MY-STREET-ROUTER-PLANNER")
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}

	// load config from environment.
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}

	log.Infow("service started")

	errs := make(chan error, errorChan)
	listenForInterrupt(errs)

	go func() {
		errs <- mystreetrouteplanerapi.Run(log, cfg)
	}()

	c := <-errs
	log.Infow("Terminating:", "Interrupt", c)

	defer loggerSync(log)
}

// loggerSync flushes any buffered log entries.
func loggerSync(log *zap.SugaredLogger) {
	if err := log.Sync(); err != nil {
		log.Error(err)
	}
}

// listenForInterrupt listen interrupt system.
func listenForInterrupt(errChan chan error) {
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		<-c

		// nolint: goerr113
		errChan <- fmt.Errorf("received end signal")
	}()
}
