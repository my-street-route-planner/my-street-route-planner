// Package config content struct Config. The function will parse the environment variables to run the app.
package config

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/my-street-route-planner/internal/business/sys/validate"
)

type Config struct {
	Environment string
	Release     string
}

// LoadConfig loads environment vars into Config.
func LoadConfig() (Config, error) {
	flags, err := getFlags()
	if err != nil {
		return Config{}, err
	}

	if checkFlags(flags) {
		return LoadConfigFile(flags["configFile"])
	}

	return LoadConfigEnvironments()
}

// getFlags reads flags.
func getFlags() (map[string]string, error) {
	var flags map[string]string

	rootCmd := new(cobra.Command)
	rootCmd.Use = "myc-cloud-app"
	rootCmd.Run = func(cmd *cobra.Command, args []string) {}

	rootCmd.Flags().StringToStringVarP(&flags, "flag", "f", nil, "Flag")

	if err := viper.BindPFlag("flag", rootCmd.Flags().Lookup("flag")); err != nil {
		return flags, fmt.Errorf("%w", validate.WrapErrFound(err, "missing key flag"))
	}

	if err := rootCmd.Execute(); err != nil {
		return flags, fmt.Errorf("%w", validate.WrapErrFound(err, "failed command execute"))
	}

	return flags, nil
}

// checkFlags checkout a configFile.
func checkFlags(flags map[string]string) bool {
	if len(flags) == 0 {
		return false
	}

	if flags["configFile"] == "" || len(flags["configFile"]) == 0 {
		return false
	}

	return true
}

// LoadConfigFile load environment vars into Config which yaml file.
func LoadConfigFile(configFile string) (Config, error) {
	var config Config

	viper.AddConfigPath(".")
	viper.SetConfigName(configFile)

	if err := viper.ReadInConfig(); err != nil {
		return config, fmt.Errorf("%w", validate.WrapErrFound(err, "this file not exist"))
	}

	if err := viper.Unmarshal(&config); err != nil {
		return config, fmt.Errorf("%w", validate.WrapErrFound(err, "unable to decode into struct"))
	}

	return config, nil
}

// LoadConfigEnvironments loads environment vars into Config.
func LoadConfigEnvironments() (Config, error) {
	var config Config

	viper.SetEnvPrefix("MY_STREET_ROUTE_PLANNER")

	if err := viper.BindEnv("ENVIRONMENT"); err != nil {
		return config, fmt.Errorf("%w", validate.WrapErrFound(err, "missing key ENVIRONMENT"))
	}

	if err := viper.BindEnv("RELEASE"); err != nil {
		return config, fmt.Errorf("%w", validate.WrapErrFound(err, "missing key RELEASE"))
	}

	if err := viper.Unmarshal(&config); err != nil {
		return config, fmt.Errorf("%w", validate.WrapErrFound(err, "unable to decode into struct"))
	}

	return config, nil
}
