package validate

import "github.com/pkg/errors"

// errFound return error.
type errFound struct {
	error
}

// WrapErrFound wrap error.
func WrapErrFound(err error, format string, args ...interface{}) error {
	return &errFound{errors.Wrapf(err, format, args...)}
}
