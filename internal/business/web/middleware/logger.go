// Package middleware contains the logger request/response.
package middleware

import (
	"time"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

// ZapLogger middleware logger. Use zapLogger library.
func ZapLogger(log *zap.SugaredLogger) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			start := time.Now()
			req, res := ctx.Request(), ctx.Response()
			requestID := getRequestID(ctx)

			log.Infow("request started", "traceid", requestID, "method", req.Method,
				"path", req.URL.Path, "remoteaddr", req.RemoteAddr)

			if err := next(ctx); err != nil {
				ctx.Error(err)
			}

			log.Infow("request completed", "traceid", requestID, "method", req.Method,
				"path", req.URL.Path, "remoteaddr", req.RemoteAddr, "statuscode", res.Status,
				"exec_time", time.Since(start).String())

			return nil
		}
	}
}

// getRequestID get request id.
func getRequestID(ctx echo.Context) string {
	if ctx.Request().Header.Get(echo.HeaderXRequestID) != "" {
		return ctx.Request().Header.Get(echo.HeaderXRequestID)
	}

	if ctx.Response().Header().Get(echo.HeaderXRequestID) != "" {
		return ctx.Response().Header().Get(echo.HeaderXRequestID)
	}

	return "00000000-0000-0000-0000-000000000000"
}
