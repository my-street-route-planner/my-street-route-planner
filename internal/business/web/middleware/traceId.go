// Package middleware contains the trace id from request.
package middleware

import (
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

// GenerateTraceID middleware generate trace id from library uuid. Add traceId into request.
func GenerateTraceID() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			traceID := uuid.New().String()
			ctx.Request().Header.Set(echo.HeaderXRequestID, traceID)

			if err := next(ctx); err != nil {
				ctx.Error(err)
			}

			return nil
		}
	}
}
