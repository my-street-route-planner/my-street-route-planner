// Package logger provides a convince function to construct a logger for use.
// This is required not just for applications but for testing.
package logger

import (
	"fmt"

	"gitlab.com/my-street-route-planner/internal/business/sys/validate"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewLogger(serviceName string) (*zap.SugaredLogger, error) {
	config := zap.NewProductionConfig()
	config.OutputPaths = []string{"stdout"}
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.DisableStacktrace = true
	config.InitialFields = map[string]interface{}{"service": serviceName}

	log, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("%w", validate.WrapErrFound(err, "failed in construct logger"))
	}

	return log.Sugar(), nil
}
